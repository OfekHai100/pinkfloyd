import socket

SERVER_IP = 'localhost'
SERVER_PORT = 70
REQ_FORMAT = "<type>.@.<name>"
QUIT_MSG = "QUIT"
ERROR_MSG = "ERROR"
WRONG_PASS = "Wrong password, disconnecting"
MENU = "1. Album list\n2. Song list in album\n3. Song length\n4. Get song lyrics\n5. What album is this song in?\n6. Find song by name\n7. Find song by lyrics\n8. The 50 most common words from all of the songs, and how many times each of them appear\n9. Album list from the longest album to the shortest album (including how many songs per album)\n10. Quit\n"
MENU_DICT = {
    1: "ALB_list",
    2: "SNG_list",
    3: "SNG_len",
    4: "SNG_lyr",
    5: "SNGF_alb",
    6: "SNGF_name",
    7: "SNGF_lyr",
    8: "SNGLC_list",
    9: "ALBL_list"
}

def login(sock):
    password = input()
    sock.sendall(password.encode())
    return get_ans(sock)

def menu_choice():
    """
    This function returns the choice of the user and makes sure that the entered value is int type(by while of try and except).
    :type choice: int
    :type error: bool
    :return: the choice from the user
    :rtype: int
    """
    print(MENU)
    error = True
    while error == True: #try and except of invalid choice
        try:
            choice = int(input("Enter your choice: "))
            error = False
        except Exception:
            print("\nEnter a valid type of request!\n")
            print(MENU)
    return choice

def menu(sock):
    """
    This is the menu function which gets the user's choice and creates the request, then sends it to the server and print its answer (if the choice is invalid, it prints error)
    :param sock: sock value
    :type choice: int
    :type req_msg: str
    :return: none
    """
    choice = menu_choice()
    while choice != 10:
        if choice == 1 or choice == 8 or choice == 9:  # Album list with no search
            req_msg = REQ_FORMAT.replace("<type>", MENU_DICT[choice])
        elif choice == 2:  # Search by album name
            req_msg = REQ_FORMAT.replace("<type>", MENU_DICT[choice])
            req_msg = req_msg.replace("<name>", input("Enter album name: "))
        elif choice == 3 or choice == 4 or choice == 5:  # Search by song name
            req_msg = REQ_FORMAT.replace("<type>", MENU_DICT[choice])
            req_msg = req_msg.replace("<name>", input("Enter song name: "))
        elif choice == 6 or choice == 7:  # Search by word
            req_msg = REQ_FORMAT.replace("<type>", MENU_DICT[choice])
            req_msg = req_msg.replace("<name>", input("Enter the word: "))
        else:
            req_msg = ERROR_MSG
            print("\nEnter a valid type of request!\n")
        if req_msg != ERROR_MSG: #Error msg
            sock.sendall(req_msg.encode())
            print("\nServer says:", get_ans(sock), "\n")
        choice = menu_choice()


def get_ans(sock):
    """
    This function gets the socket and returns answer from the server.
    :param sock: sock value
    :type server_msg: str
    :return: the answer from the server
    :rtype: str
    """
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    return server_msg


def client():
    """
    This function creates socket, connects to the server and returns it.
    :return: the client socket
    :rtype: socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    return sock

def main():
    try:
        sock = client()
        print(get_ans(sock))
        if login(sock):
            menu(sock)
            sock.sendall(QUIT_MSG.encode())
            print("Goodbye!")
        else:
            print(WRONG_PASS)
    except Exception:
        print("Start the server first!")
    sock.close()


if __name__ == "__main__":
    main()
