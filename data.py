import collections

FILE_PATH = "Pink_Floyd_DB.txt"
ALB_DELI = '#'
SNG_DELI = '*'
SEP_DELI = "::"
with open(FILE_PATH, 'r') as file:  # Use file to refer to the file object
    DB = file.read()


def alb_list():  # 1
    """
    This function returns the album list.
    :type alb_list: list
    :type albs: list
    :return: The album list
    :rtype: str
    """
    alb_list = list()
    albs = DB.split(ALB_DELI)
    albs.remove(albs[0])
    for i in range(len(albs)):
        alb_list.append(albs[i].split(SEP_DELI)[0])
    return ', '.join(alb_list)


def sng_list(value):  # 2
    """
    This function returns the songs list of an album.
    :param value: value value
    :type alb_list: list
    :type albs: list
    :return: the list of the songs in the album
    :rtype: str
    """
    alb_list = list()
    albs = DB.split(ALB_DELI)
    albs.remove(albs[0])
    for i in albs:
        if value == i.split(SEP_DELI)[0]:
            albs = i
    albs = albs.split(SNG_DELI)
    albs.remove(albs[0])
    for i in albs:
        alb_list.append(i.split(SEP_DELI)[0])
    return ', '.join(alb_list)


def sng_len(value):  # 3
    """
    This function returns the length of the song(value)
    :param value: value value
    :type len: str
    :type sngs: list
    :return: the length of the song
    :rtype: str
    """
    len = ""
    sngs = DB.split(SNG_DELI)
    sngs.remove(sngs[0])
    for i in sngs:
        if i.split(SEP_DELI)[0] == value:
            len = i.split(SEP_DELI)[2]
    return len


def sng_lyr(value):  # 4
    """
    This function returns the lyrics of the song(value).
    :param value: value value
    :type lyrics: str
    :type sng_list: list
    :return: the lyrics of the song
    :rtype: str
    """
    lyrics = ""
    sng_list = DB.split(SNG_DELI)
    sng_list.remove(sng_list[0])
    for i in sng_list:
        if i.split(SEP_DELI)[0] == value:
            lyrics = i.split(SEP_DELI)[3]
    return lyrics


def sngf_alb(value):  # 5
    """
    This function returns the name of the album the song is in.
    :param value: value value
    :type alb: str
    :type tmp: list
    :type albs: list
    :return: the name of the album the song is in
    :rtype: str
    """
    alb = ""
    tmp = list()
    albs = DB.split(ALB_DELI)
    albs.remove(albs[0])
    for i in range(len(albs)):
        tmp.append(albs[i].split(SNG_DELI))
    for i in range(len(tmp)):
        for j in range(1, len(tmp[i])):
            if tmp[i][j].split(SEP_DELI)[0] == value:
                alb = tmp[i][0].split(SEP_DELI)[0]
    return alb


def sngf_name(value):  # 6
    """
    This function searches songs by the value even if it's not accurate(not the exact letters or cases).
    :param value: value value
    :type value: str
    :type sng_list: list
    :type sngs: list
    :return: list of the songs found
    :rtype: str
    """
    value = value.lower()
    sng_list = list()
    sngs = DB.split(SNG_DELI)
    sngs.remove(sngs[0])
    for i in sngs:
        if value in i.split(SEP_DELI)[0].lower():
            sng_list.append(i.split(SEP_DELI)[0])
    return ', '.join(sng_list)


def sngf_lyr(value):  # 7
    """
    This function finds songs by only a text that is in its lyrics.
    :param value: value value
    :type value: str
    :type lyrics_list: list
    :type sng_list: list
    :return: the songs with the text in the lyrics
    :rtype: str
    """
    value = value.lower()
    lyrics_list = list()
    sng_list = DB.split(SNG_DELI)
    sng_list.remove(sng_list[0])
    for i in sng_list:
        if value in i.split(SEP_DELI)[3].lower():
            lyrics_list.append(i.split(SEP_DELI)[0])
    return ', '.join(lyrics_list)


def snglc_list(): #8
    """
    This function returns a list of all 50 most common words from all of the songs, and how many times each of them appear. It uses the collections module which helps with finding most common words in a str.
    :type lyrics: str
    :type sng_list: list
    :type counter: list
    :type most_occur: list
    :return: 50 most common words from all of the songs, and how many times each of them appear
    :rtype: str
    """
    lyrics = ""
    sng_list = DB.split(SNG_DELI)
    sng_list.remove(sng_list[0])
    for i in sng_list:
        lyrics = lyrics + "\n" + i.split(SEP_DELI)[3]
    lyrics = lyrics.lower().split()
    counter = collections.Counter(lyrics)
    most_occur = counter.most_common(50)
    return str(most_occur).strip('[]')


def albl_list(): #9
    """
    This function returns an album list from the longest album to the shortest album by songs(including how many songs per album).
    :type alb_list: list
    :type albs: list
    :type long_to_short: list
    :return: an album list from the longest album to the shortest album by songs(including how many songs per album)
    :rtype: str
    """
    alb_list = list()
    albs = DB.split(ALB_DELI)
    albs.remove(albs[0])
    long_to_short = list()
    for i in albs:
        long_to_short.append(i.split(SNG_DELI))
    long_to_short = sorted(long_to_short, key = len, reverse = True) #sort by songs per album
    for i in long_to_short:
        alb_list.append((i[0].split("::")[0], len(i) - 1))
    return str(alb_list).strip('[]')

