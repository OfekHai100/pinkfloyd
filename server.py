import socket
import data
import hashlib

LISTEN_PORT = 70
WELCOME_MSG = "Welcome, Please enter the password: "
QUIT_MSG = "QUIT"
DELI = '.@.'
ERROR_MSG = "Couldn't find this value"
PASS = "PASSWORD"
MD5_PASS = hashlib.md5(PASS.encode()).hexdigest() #hashed server password
MENU_DICT = {
    1: "ALB_list",
    2: "SNG_list",
    3: "SNG_len",
    4: "SNG_lyr",
    5: "SNGF_alb",
    6: "SNGF_name",
    7: "SNGF_lyr",
    8: "SNGLC_list",
    9: "ALBL_list"
}


def login(password):
    """
    This function valids the password sent by the client by hashing it with MD5 and compares it to the password of the server("PASSWORD")
    :param password: password value
    :return: True if the hashed password equals and False if it is not
    :rtype: bool
    """
    return hashlib.md5(password.encode()).hexdigest() == MD5_PASS


def communicate(client_soc):
    """
    This function sends and recieve msgs from and to server if the entered password it true.
    :param client_soc: client_soc value
    :type client_msg: str
    :return: none
    """
    client_soc.sendall(WELCOME_MSG.encode())  # Welcome msg
    client_msg = get_msg(client_soc)
    if login(client_msg):
        while client_msg != QUIT_MSG:
            client_soc.sendall(create_ans(client_msg).encode())
            client_msg = get_msg(client_soc)
    else:
        client_soc.sendall(False.encode())


def get_msg(client_soc):
    """
    This function gets the socket of the client and returns the msg from the client.
    :param client_soc: client_soc value
    :type client_msg: str
    :return: the msg from the client
    :rtype: str
    """
    client_msg = client_soc.recv(1024)
    client_msg = client_msg.decode()
    return client_msg


def create_ans(client_msg):
    """
    This function creates the answer of the server by the type of the request in the client msg.
    :param client_msg: client_msg value
    :type type: str
    :type name: str
    :type ans_msg: str
    :return: the answer of the server
    :rtype: str
    """
    try:
        type = client_msg.split(DELI)[0]
        name = client_msg.split(DELI)[1]
        if type == MENU_DICT[1]:
            ans_msg = data.alb_list()
        elif type == MENU_DICT[2]:
            ans_msg = data.sng_list(name)
        elif type == MENU_DICT[3]:
            ans_msg = data.sng_len(name)
        elif type == MENU_DICT[4]:
            ans_msg = data.sng_lyr(name)
        elif type == MENU_DICT[5]:
            ans_msg = data.sngf_alb(name)
        elif type == MENU_DICT[6]:
            ans_msg = data.sngf_name(name)
        elif type == MENU_DICT[7]:
            ans_msg = data.sngf_lyr(name)
        elif type == MENU_DICT[8]:
            ans_msg = data.snglc_list()
        elif type == MENU_DICT[9]:
            ans_msg = data.albl_list()
        if ans_msg == "":
            ans_msg = ERROR_MSG
    except Exception:
        ans_msg = ERROR_MSG
    return ans_msg


def listenning():
    """
    This is the listenning function of the server that gets msgs from the client and sends back answers.
    :return: none
    """
    try:
        # Create a TCP/IP socket
        listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Binding to local port 80
        server_address = ('', LISTEN_PORT)

        listening_sock.bind(server_address)

        # Listen for incoming connections
        listening_sock.listen(1)

        # Create a new conversation socket
        client_soc, client_address = listening_sock.accept()

        communicate(client_soc)

        listening_sock.close()
    except Exception:  # if the client aborts the communication not properly, close the socket with no error
        listening_sock.close()


def main():
    listenning()


if __name__ == "__main__":
    main()
